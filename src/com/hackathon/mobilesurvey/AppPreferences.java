package com.hackathon.mobilesurvey;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class AppPreferences {
	private static final String TAG = "AppPreference";
	private static final String APP_SHARED_PREFS = "com.hackathon.mobilesurvey_prefs";
	private static final String VALIDATED = "validated";
	private SharedPreferences appSharedPrefs;
	private Editor prefsEditor;

	private Context context;

	private String LOGGED_IN = "loggedIn";

	private String ACCESS_TOKEN = "access_token";
	private String ACCESS_EXPIRES = "access_expires";
	private String FACEBOOK_NAME = "facebook_name";
	private String EMAIL = "email";
	private String FACEBOOK_ID = "id";

	public static String DEFAULT_FB_NAME = "user";
	public static String DEFAULT_FB_ID = "0";
	public static String DEFAULT_EMAIL = "email@address";

	public AppPreferences(Context context) {
		
		this.appSharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
				Context.MODE_PRIVATE);
		this.prefsEditor = appSharedPrefs.edit();
	
		this.context = context;
	}

	public String getAccessTokenOfFacebook() {
		Log.d(TAG,
				"access token" + appSharedPrefs.getString(ACCESS_TOKEN, null));
		return appSharedPrefs.getString(ACCESS_TOKEN, null);

	}

	public void setAccessTokenOfFacebook(String value) {
		prefsEditor.putString(ACCESS_TOKEN, value);
		prefsEditor.commit();
	}

	public long getAccessExpiresOfFacebook() {
		return appSharedPrefs.getLong(ACCESS_EXPIRES, 0);
	}

	public void setAccessExpiresOfFacebook(long value) {
		prefsEditor.putLong(ACCESS_EXPIRES, value);
		prefsEditor.commit();
	}

	/**
	 * 
	 * @return return 0 if user is not yet registered
	 */
	public String getFacebookId() {
		return appSharedPrefs.getString(FACEBOOK_ID, DEFAULT_FB_ID);
	}

	public void setFacebookId(String value) {
		prefsEditor.putString(FACEBOOK_ID, value);
		prefsEditor.commit();
	}

	/**
	 * 
	 * @return returns "user" if not logged
	 */
	public String getFacebookName() {
		return appSharedPrefs.getString(FACEBOOK_NAME, DEFAULT_FB_NAME);
	}

	public void setFacebookName(String value) {
		prefsEditor.putString(FACEBOOK_NAME, value);
		prefsEditor.commit();
		Log.d(TAG, "facebook name is set");
	}
	

	/**
	 * 
	 * @return returns "user" if not logged
	 */
	public String getEmail() {
		return appSharedPrefs.getString(EMAIL, DEFAULT_EMAIL);
	}

	public void setEmail(String value) {
		prefsEditor.putString(EMAIL, value);
		prefsEditor.commit();
		Log.d(TAG, "facebook name is set");
	}

	public void setLoggedIn(boolean value) {
		prefsEditor.putBoolean(LOGGED_IN, value);
		prefsEditor.commit();
	}

	public boolean isLoggedIn() {
		return appSharedPrefs.getBoolean(LOGGED_IN, true);
	}
	public void setValidated(boolean value) {
		prefsEditor.putBoolean(VALIDATED, value);
		prefsEditor.commit();
	}
	
	public boolean isValidated() {
		return appSharedPrefs.getBoolean(VALIDATED, true);
	}

	public void removeFbDetails() {
		prefsEditor.remove(LOGGED_IN);
		prefsEditor.remove(ACCESS_TOKEN);
		prefsEditor.remove(ACCESS_EXPIRES);
		prefsEditor.remove(FACEBOOK_ID);
		prefsEditor.remove(FACEBOOK_NAME);
		prefsEditor.remove(EMAIL);
		prefsEditor.commit();
	}

}
