package com.hackathon.mobilesurvey;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class SplashActivity extends Activity {

	ImageView imgView;
	private static final int CHANGE_IMAGE=1;
	private static final int NEXT_ACTIVITY=2;
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			if(msg.what ==CHANGE_IMAGE){
			imgView.setImageResource(R.drawable.survey);
			}else if(msg.what == NEXT_ACTIVITY){
				startActivity(new Intent(getApplicationContext(), HomeActivity.class));
				SplashActivity.this.finish();
			}
		};
	};
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_logo);
		imgView = (ImageView) findViewById(R.id.splash_logo);
		
		Thread t = new Thread(new  Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.sendEmptyMessage(CHANGE_IMAGE);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				handler.sendEmptyMessage(NEXT_ACTIVITY);
				
			}
		});
		t.start();

		

	}

}
