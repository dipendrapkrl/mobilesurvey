package com.hackathon.mobilesurvey;

import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public class HomeActivity extends Activity {

	private static String TAG = "AuthenticationActivity";

	private static final int FACEBOOK_LOGIN = 4;

	private static final int FACEBOOK_LOGOUT = 0;

	protected static final int SUCCESS = 0;

	protected static final int FAILURE = 3;

	private AppPreferences appPreferences;
	private Miscellaneous misc;
	AsyncTaskRegistration asyRegistration;

	private FbDetails details;
	private Facebook facebook = new Facebook("339133709510762");

	private Button btnFacebook;
	private TextView facebookText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_activity);

		btnFacebook = (Button) findViewById(R.id.BtnFacebook);
		facebookText = (TextView) findViewById(R.id.BtnFacebookName);

		appPreferences = new AppPreferences(this);

		details = new FbDetails(this, this);
		misc = new Miscellaneous(this);
		boolean isValidated = appPreferences.isValidated();

		if (!isValidated) {

			btnFacebook.setText("Login");
		} else {
			btnFacebook.setText("Logout");
			facebookText.setText("Welcome "+appPreferences.getFacebookName());

		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		// activity result comes from Facebook activity
		facebook.authorizeCallback(requestCode, resultCode, data);
	}

	public void clickHandler(View v) {

		switch (v.getId()) {
		case R.id.BtnSurvey:
			if (appPreferences.isValidated()) {
				startActivity(new Intent(this, SurveyActivity.class));
			} else {
				Toast.makeText(getApplicationContext(),
						"Please Login for validation", Toast.LENGTH_LONG)
						.show();
			}
			break;

		case R.id.BtnAccount:
			if (appPreferences.isValidated()) {
				startActivity(new Intent(this, AccountActivity.class));
			} else {
				Toast.makeText(getApplicationContext(),
						"Please Login for validation", Toast.LENGTH_LONG)
						.show();
			}
			break;
		case R.id.BtnFacebook:

			if (misc.isOnline()) {
				Log.d(TAG, "isonline");
				if (!appPreferences.isLoggedIn()) {

					facebookAuthentication();

				} else if (!appPreferences.isValidated()) {
					showDialog(FACEBOOK_LOGIN);
					asyRegistration = new AsyncTaskRegistration();
					asyRegistration.execute(1);

				} else {

					showDialog(FACEBOOK_LOGOUT);

				}
			} else {
				misc.displayToast("Internet is not connected");
			}
			break;

		}
	}

	public void facebookLogout() {
		AppPreferences appPreferences = new AppPreferences(this);
		appPreferences.removeFbDetails();
		appPreferences.setLoggedIn(false);
		appPreferences.setValidated(false);
		Toast.makeText(this, " Successfully Logged Out", Toast.LENGTH_LONG)
				.show();
		facebookText.setText("Login Please ");
	}

	public void facebookAuthentication() {
		String access_token = appPreferences.getAccessTokenOfFacebook();
		long expires = appPreferences.getAccessExpiresOfFacebook();

		if (access_token != null) {
			facebook.setAccessToken(access_token);
			misc.displayToast("already available" + access_token);

		}
		if (expires != 0) {
			facebook.setAccessExpires(expires);
			misc.displayToast("already available" + expires);

		}

		if (!facebook.isSessionValid()) {

			Log.d(TAG, "session was not valid");
			facebook.authorize(this, new String[] { "email" },
					new DialogListener() {

						@Override
						public void onComplete(Bundle values) {
							misc.displayToast("Login Successful");
							appPreferences.setAccessTokenOfFacebook(facebook
									.getAccessToken());
							appPreferences.setAccessExpiresOfFacebook(facebook
									.getAccessExpires());
							appPreferences.setLoggedIn(true);
							

							final FbDetails details = new FbDetails(
									getApplicationContext(), HomeActivity.this);

							asyRegistration = new AsyncTaskRegistration();
							asyRegistration.execute(1);

						}

						@Override
						public void onFacebookError(FacebookError e) {
							Log.d(TAG, e.toString());
							misc.displayToast("Error Logging To Facebook");
						}

						@Override
						public void onError(DialogError e) {
							Log.d(TAG, e.toString());
							misc.displayToast("Error Logging To Facebook");

						}

						@Override
						public void onCancel() {
							Log.d(TAG, "cancelled");
							misc.displayToast("Error Logging To Facebook");

						}

					});
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {

		switch (id) {

		case FACEBOOK_LOGIN:
			ProgressDialog pdiDialog = ProgressDialog.show(this, "Please Wait",
					"Registering in Server...");
			pdiDialog.setCancelable(true);
			pdiDialog.setOnCancelListener(new OnCancelListener() {

				@Override
				public void onCancel(DialogInterface dialog) {
					// TODO Auto-generated method stub
					if (asyRegistration != null)
						if (asyRegistration.cancel(true))
							;
				}
			});

			return pdiDialog;

		case FACEBOOK_LOGOUT:
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle("Are You Sure?");
			builder.setMessage("Removes All Datas and Closes the Application");
			builder.setPositiveButton("Ok",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							dismissDialog(FACEBOOK_LOGOUT);
							facebookLogout();

							//HomeActivity.this.finish();
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							dismissDialog(FACEBOOK_LOGOUT);

						}
					});
			return builder.create();

		default:
			return null;
		}

	}

	private class AsyncTaskRegistration extends
			android.os.AsyncTask<Integer, Integer, Integer> {

		String email, userid, name;
		boolean status = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			showDialog(FACEBOOK_LOGIN);

		}

		@Override
		protected Integer doInBackground(Integer... params) {
			// TODO Auto-generated method stub
			HashMap<String, String> values = details
					.getAndSetUserDetails(appPreferences
							.getAccessTokenOfFacebook());
			if (values != null) {
				email = values.get("email");
				userid = values.get("id");
				name = values.get("name");

				publishProgress(1);

				status = new ServerSideHelper().sendCredentials(
						appPreferences.getFacebookName(),
						appPreferences.getFacebookId(),
						appPreferences.getEmail());
			}

			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {

			super.onProgressUpdate(values);
			appPreferences.setEmail(email);
			appPreferences.setFacebookId(userid);
			appPreferences.setFacebookName(name);
			facebookText.setText("Welcome "+ appPreferences.getFacebookName());

		}

		@Override
		protected void onPostExecute(Integer result) {
	
			dismissDialog(FACEBOOK_LOGIN);
			super.onPostExecute(result);
			if (status) {
				appPreferences.setValidated(true);

				btnFacebook.setText("Logout");

			} else {

				Toast.makeText(getApplicationContext(),
						"Failed to Register, Login Again", Toast.LENGTH_LONG)
						.show();
			}

		}
	}

}
