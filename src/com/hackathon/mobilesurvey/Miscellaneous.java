package com.hackathon.mobilesurvey;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.widget.Toast;


public class Miscellaneous {

	private static final String TAG = "Miscellaneous";
	
	Context context;
	
	static String MAP_PACKAGE_NAME = "com.google.android.apps.maps";
	private static boolean enableLog = true; 
	private final static int PRECISION = 3;
	
	public Miscellaneous(Context context){
		this.context = context;
	}

	/**
	 * checks whether the specified package is installed or not
	 * @param packageName the name of the package to be checked 
	 * @return true if package is available in the system
	 */
	public boolean isAppInstalled(String packageName) {
		boolean isInstalled = false;
		PackageManager pm = context.getPackageManager();
		try {
			//following line is expected to throw exception if package is not available
			pm.getApplicationInfo(packageName, 0);
			isInstalled = true;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
			isInstalled = false;
		}
		
		return isInstalled;
	}
	
	
	
	
	/**checks whether Internet is available or not
	 * 
	 * @return true if Internet is connected or is connecting
	 */
	public  boolean  isOnline() {
		
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return (netInfo != null && netInfo.isConnectedOrConnecting())?true:false;
	}
	
	
	/**
	 * 
	 * @param context context of displaying toast
	 * @param message message to be displayed
	 */
	public void displayToast(String message) {
		 
	new Toast(context);
	Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	
	public void logd(String TAG, String message){
		if(enableLog){
			Log.d(TAG, message);
		}
	}
	public void logv(String TAG, String message){
		if(enableLog){
			Log.v(context.getClass().getSimpleName(), message);
		}
	}

	public void loge(String tAG2, String message) {
		if(enableLog){
			Log.e(context.getClass().getSimpleName(), message);
		}		
	}

	
	public String[][] getStringArrayFromFloat(float[][]  x ) {
		//changing float array to string array
		String[][] val = new String[x.length][x[0].length];
		for (int i = 0; i < x.length; i++) {
			for (int j = 0; j < x[0].length; j++) {
				val[i][j] = x[i][j]+"";
			}
		}
		return val;

	}

	
	/**
	 * 
	 * @return year-month only eg 2012-00 for jan of 2012
	 */
	public String getYearMonth() {
		// TODO Auto-generated method stub
		logd(TAG,"Here");
		Calendar cal = Calendar.getInstance();
		
		int month = cal.get(Calendar.MONTH);
		String month1 = month<=9?"0"+month:month+"";
		String date = cal.get(Calendar.YEAR)+"-"+month1;
		logd(TAG,date);
		return date;
	}
	
	
	/**
	 * 
	 * @return  current year-month-day 
	 * remember eg. 2012-00-01 for jan first of 2012 
	 */
	public String getYearMonthDay() {
		Calendar cal = Calendar.getInstance();
		
		int month = cal.get(Calendar.MONTH);
		String month1 = month<=9?"0"+month:month+"";
		int day = cal.get(Calendar.DATE);
		String day1 = day<=9?"0"+day:Integer.toString(day);
		
		String date = cal.get(Calendar.YEAR)+"-"+month1 + "-" + day1;
		logd(TAG,date);
		return date;
	}
	
	/**
	 * 
	 * @return fully qualified datetime string XXXX-XX-XX XX-XX-XX   
	 * Remember the space between date and time too
	 * remember  eg. O0 for jan
	 */
	public String getYearMonthDayHMS() {
		Calendar cal = Calendar.getInstance();
		
		int month = cal.get(Calendar.MONTH);
		String month1 = month<=9?"0"+month:month+"";
		int day = cal.get(Calendar.DATE);
		String day1 = day<=9?"0"+day:Integer.toString(day);
		
		int hour =cal.get(Calendar.HOUR);
		int min =cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		String hour1 = day<=9?"0"+hour:Integer.toString(hour);
		String min1 = min<=9?"0"+min:Integer.toString(min);
		String sec1 = sec<=9?"0"+sec:Integer.toString(sec);
		String date = cal.get(Calendar.YEAR)+"-"+month1 + "-" + day1 +" "+hour1 + "-"+min1 + "-"+sec1;
		logd(TAG,date);
		
		return date;
	}
	
	/**
	 * 
	 * @param year eg 1988
	 * @param month eg 0
	 * @param day eg 1
	 * @param hour eg 1
	 * @param min eg 1
	 * @param sec eg 1
	 * @return eg 1988-00-01 01-01-01
	 * remember the space between date and time
	 */
	
	public String getQualifiedDateTime(int year, int month, int day, int hour, int min, int sec) {

		String date;
		
		
		String month1 = month<=9?"0"+month:month+"";
		String day1 = day<=9?"0"+day:Integer.toString(day);
		
		
		String hour1 = day<=9?"0"+hour:Integer.toString(hour);
		String min1 = min<=9?"0"+min:Integer.toString(min);
		String sec1 = sec<=9?"0"+sec:Integer.toString(sec);
		date = year+"-"+month1 + "-" + day1 +" "+hour1 + "-"+min1 + "-"+sec1;
		logd(TAG,date);
		
	
		return date;
	}
	
	 
	/**
	 * 
	 * @param year eg 1933
	 * @param monthOfYear eg 0 for jan
	 * @param dayOfMonth eg  eg 1 for 1
	 * @return  eg. of above parameters result will be 1933-00-01
	 */
	public String getQualifiedDate(int year, int monthOfYear, int dayOfMonth) {
		
		String selectedDate ;
		String qualifiedMonth = (monthOfYear ) <= 9 ? ("0" + (monthOfYear ))
				: ((monthOfYear ) + "");
		String qualifiedDay = ((dayOfMonth) <= 9 ? ("0" + (dayOfMonth))
				: dayOfMonth + "");

		 selectedDate = year + "-" + qualifiedMonth
				+ "-" + qualifiedDay;
		
		 return selectedDate;
		
	}
	
	/**
	 * used to format the distance to some decimal places
	 * @param distance distance eg 1223.23232312332
	 * @return 1223.232
	 */
	public static String format(String distance) {
		
		
		if(distance == null) return null;
		int dotPosition= distance.indexOf(".");
		
		if(dotPosition == -1)return distance;
		if(distance.substring(dotPosition).length()<(PRECISION+1)){
			return distance;
		}
	
			return distance.substring(1, dotPosition+PRECISION+1);
		
		
		
	}
	
	/**
	 * formats second time into hour min and sec
	 * @param time eg 1233 
	 * @return eg 04:09:03
	 */
	public static String timeFormatter(Integer time, boolean doHideSecondField) {
		
		String hour = "00" ;
		String min = "00" ;
		String sec = "00" ;
		int hr=0, m=0, s=time;
		if(s>=60){
			m = s /60;
			s = s % 60;
		}
		if(m >= 60){
			hr = m /60;
			m = m %60;
		}
		
		sec =(s <= 9) ? "0"+s: s+"";
		min =(m <= 9) ? "0"+m: m+"";
		hour =(hr <= 9) ? "0"+hr: hr+"";
		
		if(doHideSecondField){
					return (hour + ":"+min);
		}else{
			
			
		return (hour + ":"+min+":"+sec);
		
		}
	}
	


	/**
	 * makes the text colorful for textview
	 * Usually done in this way:
	 * 	textView.setText(getColoredText(names, colors, true), BufferType.SPANNABLE);
	 * @param names words/names to be made colorful
	 * @param colors corresponding color for the @names
	 * @param includeSpaceInbetween true if space is desired between @names
	 * @return colored text ; null if @names and @colors size is not equal
	 * 
	 */
	public SpannableStringBuilder getColoredText(List<String> names,
			int[] colors, boolean includeSpaceInbetween) {
		SpannableStringBuilder textString = new SpannableStringBuilder();

		if (names.size() != colors.length) {
			Log.d("Error", "first two parameters should have equal size");
			return null;
		}

		List<String> names1 = new ArrayList<String>();
		if (includeSpaceInbetween) {
			for (String string : names)
				names1.add(string + " ");

		} else {
			names1 = names;
		}

		List<ForegroundColorSpan> fcolors = new ArrayList<ForegroundColorSpan>();
		for (int j = 0; j < colors.length; j++)
			fcolors.add(new ForegroundColorSpan(colors[j]));

		int[] startPositions = new int[names1.size()];
		int i = 0;
		for (String string : names1) {

			startPositions[i] = (i == 0) ? 0 : textString.length();
			textString.append(string);
			i++;
		}

		Log.d("hai", startPositions.length + "is lenget");

		int j = 0;
		for (ForegroundColorSpan foregroundColorSpan : fcolors) {
			Log.d("hai", j + "");
			textString.setSpan(foregroundColorSpan, (j == 0) ? 0
					: startPositions[j],
					(j != startPositions.length - 1) ? startPositions[j + 1]
							: textString.length(), 0);
			j++;
		}

		return textString;
	}
	
	
	
	
	
	
	
	
}
