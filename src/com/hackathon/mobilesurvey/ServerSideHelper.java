package com.hackathon.mobilesurvey;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;



public class ServerSideHelper  {
	
	

	
	
	private static final String POST_USERID = "user_fb_id";
	private static final String POST_USERNAME = "user_name";
	private static final String POST_EMAIL = "email";
	private String postSite="http://"+MainApplication.IPADDRESS+"/mobile_survey/userEntry.php";

	public boolean sendCredentials(String userName, String userId, String email) {
		
		Log.d("tag", "inside send credentials");
		DefaultHttpClient client = new DefaultHttpClient();
		ResponseHandler<String> res = new BasicResponseHandler();
		HttpPost postMethod = new HttpPost(postSite);
		postMethod.getParams().setParameter(
				CoreProtocolPNames.USE_EXPECT_CONTINUE, Boolean.FALSE);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		nameValuePairs.add(new BasicNameValuePair(POST_USERID,userId));
		nameValuePairs.add(new BasicNameValuePair(POST_USERNAME,userName));
		nameValuePairs.add(new BasicNameValuePair(POST_EMAIL,email));
				
			
		try {
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			String response = client.execute(postMethod, res);
			Log.d("server response is :::::", response);
			
			JSONObject obj = new JSONObject(response);
			String success = obj.getString("success");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			
			return false;
		} catch (ClientProtocolException e) {
		
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			
			e.printStackTrace();
			return false;
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;

	}

}
