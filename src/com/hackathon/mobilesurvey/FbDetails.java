package com.hackathon.mobilesurvey;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.Util;

public class FbDetails {

	Context context;
	private static String TAG = "Facebook";
	AppPreferences appPreferences;;
	//Miscellaneous miscellaneous;
	private String[][] mutualFriends; // id,name

	Facebook facebook = new Facebook("130516427078299");
	AsyncFacebookRunner asyncFacebookRunner = new AsyncFacebookRunner(facebook);
	Activity activity;

	

	public FbDetails(Context context, Activity activity) {
		this.context = context;
		this.activity = activity;

		appPreferences = new AppPreferences(context);
	//	miscellaneous = new Miscellaneous(context);

	}

	
	/**
	 * blocking method downloads facebook id and facebook name of the current
	 * user and saves name and id in preference
	 * 
	 * @return facebook name of the user
	 */
	public HashMap<String, String> getAndSetUserDetails(String accessToken) {
		String name = null;
		Bundle b = new Bundle();
		b.putString("access_token", accessToken);
		b.putString("query", "select uid, name,email from user where uid=me()");
		b.putString("method", "fql.query");
		String response;
		try {
			response = facebook.request(b);

			Log.d(TAG, response.toString());
			JSONArray jsonArray = new JSONArray(response);
			int size = jsonArray.length();
			final String id = jsonArray.getJSONObject(0).getString("uid");
			final String fullname = jsonArray.getJSONObject(0)
					.getString("name");
			final String email = jsonArray.getJSONObject(0).getString("email");
			name = fullname;
			HashMap<String, String> credentials = new HashMap<String,String>();
			credentials.put("id", id);
			credentials.put("name", name);
			credentials.put("email", email);
		return credentials;
			
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		} catch (JSONException e) {
		
			e.printStackTrace();
		}

		Log.d(TAG, "id and name is set");
		return null;

	}

}
