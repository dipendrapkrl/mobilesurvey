package com.hackathon.mobilesurvey;

import java.io.IOException;
import java.net.MalformedURLException;

import com.facebook.android.Facebook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class SurveyActivity extends Activity {

	private static final int FACEBOOK_LOGOUT = 0;
	private static final String form_url="http://"+MainApplication.IPADDRESS+"/mobile_survey/formgenerate.php";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		WebView webview = new WebView(this);
		 // Let's display the progress in the activity title bar, like the
		 // browser app does.
		 getWindow().requestFeature(Window.FEATURE_PROGRESS);
		 webview.getSettings().setJavaScriptEnabled(true);
		 
		 setContentView(webview);
		 
		

		 

		 final Activity activity = this;
		 webview.setWebChromeClient(new WebChromeClient() {
		   public void onProgressChanged(WebView view, int progress) {
		     // Activities and WebViews measure progress with different scales.
		     // The progress meter will automatically disappear when we reach 100%
		     activity.setProgress(progress * 1000);
		   }
		 });
		 webview.setWebViewClient(new WebViewClient() {
		   public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
		     Toast.makeText(activity, "Oh no! " + description, Toast.LENGTH_SHORT).show();
		   }
		 });

		 webview.loadUrl(form_url+"?user_fb_id="+new AppPreferences(this).getFacebookId());
		 
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		MenuInflater inflater = new MenuInflater(this);
		inflater.inflate(R.menu.activity_survery, menu);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			startActivity(new Intent(getApplicationContext(), SettingActivity.class));
			break;
		case R.id.menu_account:
			 startActivity(new Intent(this, AccountActivity.class));
			break;
			
		case R.id.menu_logout:	
			showDialog(FACEBOOK_LOGOUT);
			
			break;

		}

		return true;
	}
	



	
}